var topic = [
    {
        title: "尚未開學",
        isStop: true
    },
    {
        title: "國定假日",
        isStop: true
    },
    {
        title: "環境準備",
        isStop: false
    },
    {
        title: "隨機性",
        isStop: false
    },
    {
        title: "重複性",
        isStop: false
    }
];

var date = new Date();


function setDate(month, day){
    date.setMonth(month - 1, day);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
}

setDate(1,1);